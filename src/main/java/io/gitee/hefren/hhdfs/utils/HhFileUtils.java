package io.gitee.hefren.hhdfs.utils;

import static com.google.common.base.Preconditions.*;
import com.google.gson.Gson;
import io.gitee.hefren.hhdfs.beans.HhFileMeta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Objects;
import java.util.UUID;

/**
 * @Date 2024/8/6
 * @Author lifei
 */
public class HhFileUtils {

    public static final String META_SUFFIX = ".meta";

    private static final Logger log = LoggerFactory.getLogger(HhFileUtils.class);

    private static final String DEFAULT_CONTENT_TYPE = "application/octet-stream";

    public static String getContentType(String fileName) {
        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String contentType = fileNameMap.getContentTypeFor(fileName);
        if (Objects.isNull(contentType) || contentType.isBlank()) {
            contentType = DEFAULT_CONTENT_TYPE;
        }
        return contentType;
    }

    public static void createDirPath(String dirPath) {
        File fileDir = new File(dirPath);
        if (!fileDir.exists()) {
            boolean ok = fileDir.mkdirs();
            log.debug("==> {} create dir: {}", ok, dirPath);
        }
    }

    public static String getUUIDFilePath(String dirName, String uuidFileName) {
        return dirName + File.separator + getSubDir(uuidFileName) + File.separator + uuidFileName;
    }

    public static String getSubDir(String uuidFileName) {
        return uuidFileName.substring(0, 2);
    }

    public static String getUUIDFileName(String fileName) {
        return UUID.randomUUID() + getExt(fileName);
    }

    public static String getExt(String fileName) {
        if (Objects.isNull(fileName) || fileName.isBlank()) {
            return "";
        }
        return fileName.substring(fileName.lastIndexOf("."));
    }

    public static void write(HhFileMeta hhFileMeta, File file) {
        try {
            Files.writeString(Paths.get(file.getAbsolutePath()), new Gson().toJson(hhFileMeta), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
        }catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void write(String content, File file) {
        try {
            Files.writeString(Paths.get(file.getAbsolutePath()), content, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
        }catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String read(File file) {
        try {
            return Files.readString(Paths.get(file.getAbsolutePath()));
        }catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void downloadFile(String url, File file) {
        RestTemplate restTemplate = new RestTemplate();
        // 空的 头信息
        HttpHeaders httpHeaders = new HttpHeaders();
        // 封装请求
        HttpEntity<?> request = new HttpEntity<>(httpHeaders);
        // 调用访问
        ResponseEntity<Resource> response = restTemplate.exchange(url, HttpMethod.GET, request, Resource.class);
        // 处理响应
        Resource body = response.getBody();
        checkState(Objects.nonNull(body), "download file fail.");
        try (InputStream inputStream = body.getInputStream();
             OutputStream outputStream = new FileOutputStream(file)){
            byte[] bytes = new byte[1024 * 16];
            int len = 0;
            while ((len=inputStream.read(bytes))!=-1) {
                outputStream.write(bytes, 0, len);
            }
            outputStream.flush();
        }catch (Exception e) {
            log.debug("====> download file fail: {}", e.getMessage());
        }
    }
}
