package io.gitee.hefren.hhdfs.conf;

import com.google.common.base.MoreObjects;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.io.File;

/**
 * @Date 2024/8/6
 * @Author lifei
 */
@Configuration
public class HhDataConf {

    @Value("${hhdfs.path}")
    private String dirPath;
    @Value("${server.port}")
    private Integer port;
    @Value("${hhdfs.backup-url}")
    private String backupURL;
    @Value("${hhdfs.auto-md5}")
    private Boolean autoMd5;
    @Value("${hhdfs.auto-backup}")
    private Boolean autoBackup;
    @Value("${hhdfs.topic}")
    private String topic;
    @Value("${hhdfs.download-url}")
    private String downloadURL;

    public String getBaseDirPath() {
        return dirPath + File.separator + port;
    }

    public String getDirPath() {
        return dirPath;
    }

    public void setDirPath(String dirPath) {
        this.dirPath = dirPath;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getBackupURL() {
        return backupURL;
    }

    public void setBackupURL(String backupURL) {
        this.backupURL = backupURL;
    }

    public Boolean getAutoMd5() {
        return autoMd5;
    }

    public void setAutoMd5(Boolean autoMd5) {
        this.autoMd5 = autoMd5;
    }

    public String getDownloadURL() {
        return downloadURL;
    }

    public void setDownloadURL(String downloadURL) {
        this.downloadURL = downloadURL;
    }

    public Boolean getAutoBackup() {
        return autoBackup;
    }

    public void setAutoBackup(Boolean autoBackup) {
        this.autoBackup = autoBackup;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(HhDataConf.class)
                .add("dirPath", dirPath)
                .add("port", port)
                .add("backupURL", backupURL)
                .add("autoMd5", autoMd5)
                .add("autoBackup", autoBackup)
                .add("topic", topic)
                .add("downloadURL", downloadURL)
                .toString();
    }
}
