package io.gitee.hefren.hhdfs.controller;

import com.google.common.base.Strings;
import io.gitee.hefren.hhdfs.beans.HhFileMeta;
import io.gitee.hefren.hhdfs.conf.HhDataConf;
import io.gitee.hefren.hhdfs.service.HhdfsHttpSyncer;
import static io.gitee.hefren.hhdfs.utils.HhFileUtils.*;

import io.gitee.hefren.hhdfs.service.HhdfsMQSyncer;
import io.gitee.hefren.hhdfs.utils.HhFileUtils;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * @Date 2024/8/4
 * @Author lifei
 */
@RestController
public class HhdfsController {

    private static final Logger log = LoggerFactory.getLogger(HhdfsController.class);


    @Resource
    private HhDataConf hhDataConf;

    @Resource
    private HhdfsHttpSyncer hhdfsHttpSyncer;

    @Resource
    private HhdfsMQSyncer hhdfsMQSyncer;

    /**
     * 上传功能
     *
     * @param file
     * @return
     */
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String upload(@RequestParam("file") MultipartFile file, HttpServletRequest httpServletRequest) {
        // 1. 处理文件
        // 创建一个目录
        String dirName = hhDataConf.getBaseDirPath();
        // 获取文件名， 判断是否需要在同节点之间进行文件同步
        String fileName = httpServletRequest.getHeader(HhdfsHttpSyncer.XFILENAME);
        String originalFileName = null;
        boolean needSync = false;
        if (Objects.isNull(fileName) || fileName.isBlank()) {
            needSync = true;
            originalFileName = file.getOriginalFilename();
            fileName = getUUIDFileName(originalFileName);
        } else {
            fileName = URLDecoder.decode(fileName, StandardCharsets.UTF_8);
            originalFileName = URLDecoder.decode(httpServletRequest.getHeader(HhdfsHttpSyncer.XORIGINALFILENAME), StandardCharsets.UTF_8);
        }
        String filePathName = getUUIDFilePath(dirName, fileName);
        File filePath = new File(filePathName);
        try {
            // 下载文件
            file.transferTo(filePath);
            log.debug("===> success upload file:{}", filePathName);
            // 2. 保存元信息
            HhFileMeta hhFileMeta = new HhFileMeta(fileName, originalFileName, filePath.length());
            hhFileMeta.setDownloadURL(hhDataConf.getDownloadURL());
            if (hhDataConf.getAutoMd5()) {
                try (InputStream inputStream = new FileInputStream(filePath)) {
                    hhFileMeta.getTags().put("md5", DigestUtils.md5DigestAsHex(inputStream));
                }
            }
            String metaName = fileName + META_SUFFIX;
            String metaFilePath = getUUIDFilePath(dirName, metaName);
            HhFileUtils.write(hhFileMeta, new File(metaFilePath));
            // 3. 同步文件
            if (needSync) {
                if (hhDataConf.getAutoBackup()) {
                    try {
                        String sync = hhdfsHttpSyncer.sync(filePath, fileName, originalFileName, hhDataConf.getBackupURL());
                        log.debug("===> success sync  file {} to {}", sync, hhDataConf.getBaseDirPath());
                    }catch (Exception e) {
                        log.debug("===> 非异步同步操作失败，转为异步同步文件");
                        hhdfsMQSyncer.sync(hhFileMeta);
                    }
                }else {
                    hhdfsMQSyncer.sync(hhFileMeta);
                }
            }
            return fileName;
        } catch (Exception e) {
            log.error("===> upload error: {}", e.getMessage());
            throw new RuntimeException(e);
        }
    }



    /**
     * 下载功能
     * 第一种下载方式： 返回void
     * 一段一段写，结合下载大文件
     * @param fileName
     * @param response
     */
    @RequestMapping(value = "/download")
    public void download(@RequestParam("fileName") String fileName, HttpServletResponse response) {
        // 文件目录
        String fileNamePath = getUUIDFilePath(hhDataConf.getBaseDirPath(), fileName);
        File file = new File(fileNamePath);
        if (!file.exists()) {
            log.debug("===> file not exists: {}", fileNamePath);
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        // 请求响应头，让响应知道 让它下载文件
        // 指定字符集
        response.setCharacterEncoding("UTF-8");
        // 指定文件类: 二进制
        // application/pdf 等等
//        response.setContentType("application/octet-stream");
        response.setContentType(getContentType(fileName));
        // 指定文件名, 编码 解决中文乱码
        String encodeFileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8);
        // Content-Disposition 有两个值： attachment 附件， inline 在浏览器打开（如果能打开）
        // "+encodeFileName+"
        response.setHeader("Content-Disposition", "inline;filename=\""+encodeFileName+"\"");
        // 指定文件长度
        response.setContentLength((int)file.length());
        // 下载
        try (FileInputStream fileInputStream = new FileInputStream(file);
             BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream)) {
            ServletOutputStream outputStream = response.getOutputStream();
            byte[] bytes = new byte[1024 * 16];
            int len = 0;
            while ((len = bufferedInputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, len);
            }
            outputStream.flush();
        } catch (Exception e) {
            log.error("===> download error: {}", e.getMessage());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * 下载功能
     * 第二种下载方式： 返回 byte[]
     * 缺点： 要先把数据内容加载到内存中，不适合下载大文件
     * @param fileName
     * @param response
     */
    @RequestMapping(value = "/downloadBytes")
    public byte[] downloadBytes(@RequestParam("fileName") String fileName, HttpServletResponse response) {
        // 文件目录
        String fileNamePath = getUUIDFilePath(hhDataConf.getBaseDirPath(), fileName);
        File file = new File(fileNamePath);
        if (!file.exists()) {
            log.debug("===> file not exists: {}", fileNamePath);
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
        // 请求响应头，让响应知道 让它下载文件
        // 指定字符集
        response.setCharacterEncoding("UTF-8");
        // 指定文件类: 二进制
        // application/pdf 等等
//        response.setContentType("application/octet-stream");
        response.setContentType(getContentType(fileName));
        // 指定文件名, 编码 解决中文乱码
        String encodeFileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8);
        // Content-Disposition 有两个值： attachment 附件， inline 在浏览器打开（如果能打开）
        // "+encodeFileName+"
        response.setHeader("Content-Disposition", "attachment;filename=\""+encodeFileName+"\"");
//        response.setHeader("Content-Disposition", "inline;filename=\""+encodeFileName+"\"");
        // 下载
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            byte[] bytes = new byte[(int)file.length()];
            int readLen = fileInputStream.read(bytes);
            log.debug("===> downloadBytes readLen: {}", readLen);
            return bytes;
        } catch (Exception e) {
            log.error("===> download error: {}", e.getMessage());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/meta", method = RequestMethod.GET)
    public String meta(@RequestParam("fileName") String fileName) {
        String metaFileNamePath = getUUIDFilePath(hhDataConf.getBaseDirPath(), fileName) + META_SUFFIX;
        File file = new File(metaFileNamePath);
        if (!file.exists()) {
            return Strings.lenientFormat("No meta fileName: %s", fileName);
        }
        return HhFileUtils.read(file);
    }

}
