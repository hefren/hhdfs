package io.gitee.hefren.hhdfs.service;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import io.gitee.hefren.hhdfs.beans.HhFileMeta;
import io.gitee.hefren.hhdfs.conf.HhDataConf;
import io.gitee.hefren.hhdfs.utils.HhFileUtils;
import jakarta.annotation.Resource;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Objects;

/**
 * 监听消息，把要同步的文件下载到本地
 * @Date 2024/8/8
 * @Author lifei
 */
@Service
@RocketMQMessageListener(topic = "${hhdfs.topic}", consumerGroup = "${hhdfs.consumer-group}")
public class HhdfsMQSyncerListener implements RocketMQListener<MessageExt> {

    private static final Logger log = LoggerFactory.getLogger(HhdfsMQSyncerListener.class);

    @Resource
    private HhDataConf hhDataConf;

    @Override
    public void onMessage(MessageExt messageExt) {
        log.debug("===> message ID: {}", messageExt.getMsgId());
        String json = new String(messageExt.getBody());
        HhFileMeta hhFileMeta = new Gson().fromJson(json, HhFileMeta.class);
        String downloadURL = hhFileMeta.getDownloadURL();
        if (Objects.isNull(downloadURL) || downloadURL.isBlank()) {
            log.debug("===> download url is blank, message: {}", json);
            return;
        }
        // 如果是当前节点，就不用再执行同步操作了
        if (hhDataConf.getDownloadURL().equals(downloadURL)) {
            log.debug("===> same server not sync!");
            return;
        }
        log.debug("===> begin sync process...");
        // 获取要写入的文件路径
        String baseDirPath = hhDataConf.getBaseDirPath();
        String filePath = HhFileUtils.getUUIDFilePath(baseDirPath, hhFileMeta.getName());
        String metaPath = filePath + HhFileUtils.META_SUFFIX;
        // 1. 把 元信息 写入到本地
        File metaFile = new File(metaPath);
        if (metaFile.exists()) {
            log.debug("===> metaFile exists, not sync!");
        }else {
            HhFileUtils.write(json, metaFile);
            log.debug("===> success write meta info: {}", metaPath);
        }

        // 2. 下载目标文件到本地
        File file = new File(filePath);
        if (file.exists() && file.length() == hhFileMeta.getSize()) {
            log.debug("===> file exists, not sync: {}", filePath);
        } else {
            String url = Strings.lenientFormat("%s?fileName=%s", downloadURL, hhFileMeta.getName());
            HhFileUtils.downloadFile(url, file);
            log.debug("===> success download file: {}", url);
        }
    }
}
