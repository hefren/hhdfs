package io.gitee.hefren.hhdfs.service;

import com.google.gson.Gson;
import io.gitee.hefren.hhdfs.beans.HhFileMeta;
import io.gitee.hefren.hhdfs.conf.HhDataConf;
import jakarta.annotation.Resource;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

/**
 * 异步同步
 * Date 2024/8/8
 * @Author lifei
 */
@Component
public class HhdfsMQSyncer {

    private static final Logger log = LoggerFactory.getLogger(HhdfsMQSyncer.class);

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    @Resource
    private HhDataConf hhDataConf;

    /**
     * 发送同步元数据到消息队列的topic
     * @param hhFileMeta
     */
    public void sync(HhFileMeta hhFileMeta) {
        Message<String> message = MessageBuilder.withPayload(new Gson().toJson(hhFileMeta)).build();
        rocketMQTemplate.send(hhDataConf.getTopic(), message);
        log.debug("===> success send message: {}", message);
    }
}
