package io.gitee.hefren.hhdfs.service;

import jakarta.annotation.Resource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * @Date 2024/8/4
 * @Author lifei
 */
@Component
public class HhdfsHttpSyncer {

    public static final String XFILENAME = "X-fileName";
    public static final String XORIGINALFILENAME = "X-original-fileName";

    @Resource
    private RestTemplate restTemplate;

    /**
     * 同步功能
     * 通过发送http请求，实现文件同步
     * @param file
     * @param fileName
     * @param url
     * @return
     */
    public String sync(File file, String fileName, String originalFileName, String url) {
        // 1. 请求头
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        // 为了解决中文乱码，编码文件名
        String encodeFileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8);
        String encodeOriginalFileName = URLEncoder.encode(originalFileName, StandardCharsets.UTF_8);
        httpHeaders.add(XFILENAME, encodeFileName);
        httpHeaders.add(XORIGINALFILENAME, encodeOriginalFileName);
        // 2. 请求体
        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("file", new FileSystemResource(file));
        // 3. 封装请求
        HttpEntity<MultiValueMap<String, HttpEntity<?>>> request = new HttpEntity<>(builder.build(), httpHeaders);
        // 4. 发送请求
        ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class);
        return response.getBody();
    }


}
