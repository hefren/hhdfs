package io.gitee.hefren.hhdfs;

import io.gitee.hefren.hhdfs.conf.HhDataConf;
import io.gitee.hefren.hhdfs.utils.HhFileUtils;
import jakarta.annotation.Resource;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.File;

@SpringBootApplication
public class HhdfsApplication {

    @Resource
    private HhDataConf hhDataConf;

    public static void main(String[] args) {
        SpringApplication.run(HhdfsApplication.class, args);
    }


    @Bean
    public ApplicationRunner runner() {
        return args->{
            // 创建根目录
            String rootDir = hhDataConf.getBaseDirPath();
            HhFileUtils.createDirPath(rootDir);
            // 创建子目录： 256个
            // 创建一个字节大小数据的目录
            // 一个字节 是 8个 bit， 可以表示 256个数字
            // 用两个 十六进制的数，刚好就能表示256个值
            for (int i = 0; i < 256; i++) {
                String subDir = String.format("%02x", i);
                String dirPath = rootDir + File.separator + subDir;
                HhFileUtils.createDirPath(dirPath);
            }

        };
    }

}
