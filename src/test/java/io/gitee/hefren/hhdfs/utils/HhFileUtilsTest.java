package io.gitee.hefren.hhdfs.utils;

import org.junit.Test;

/**
 * @Date 2024/8/6
 * @Author lifei
 */
public class HhFileUtilsTest {

    @Test
    public void getContentTypeTest() {
        String fileName = "aa.pdf";
        String contentType = HhFileUtils.getContentType(fileName);
        System.out.println(contentType);
    }
}
